/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WEATHER_H_
#define WEATHER_H_

#include <stdint.h>

typedef struct
{
  uint8_t valid_map;

  uint16_t pressure_min;
  uint16_t pressure_max;
  uint16_t pressure_avg;

  int8_t temperature_min;
  int8_t temperature_max;
  int8_t temperature_avg;

  uint8_t humidity_min;
  uint8_t humidity_max;
  uint8_t humidity_avg;

  uint16_t mq7_min;
  uint16_t mq7_max;
  uint16_t mq7_avg;

  uint16_t mq131_min;
  uint16_t mq131_max;
  uint16_t mq131_avg;

  uint16_t mics_min;
  uint16_t mics_max;
  uint16_t mics_avg;
} weather_data_t;

int
weather_telem_reset(weather_data_t *w);

uint8_t
weather_telem_valid(const weather_data_t *w);

int
weather_update_pressure(weather_data_t *w,
                        uint16_t min, uint16_t max, uint16_t avg);

int
weather_update_temperature(weather_data_t *w,
                           int8_t min, int8_t max, int8_t avg);

int
weather_update_humidity(weather_data_t *w,
                        uint8_t min, uint8_t max, uint8_t avg);

int
weather_update_mq7(weather_data_t *w,
                   uint16_t min, uint16_t max, uint16_t avg);

int
weather_update_mq131(weather_data_t *w,
                     uint16_t min, uint16_t max, uint16_t avg);

int
weather_update_mics(weather_data_t *w,
                    uint16_t min, uint16_t max, uint16_t avg);

#endif /* WEATHER_H_ */
